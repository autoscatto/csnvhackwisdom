#!/usr/bin/python
import os
import pickle
import string

from flask import Flask, request, render_template, send_from_directory
from flask_sockets import Sockets

import random
LOCAL = not 'OPENSHIFT_PYTHON_DIR' in os.environ.keys()
CORPORA = "corpora.pk"
MAX_WORDS = 1000


def get_word(r, wc):
    rd = r.random()
    cumulative = 0.0
    for word in wc:
        cumulative += wc[word]
        if cumulative > rd:
            return word

fd = open(os.path.join(os.path.dirname(__file__), CORPORA), 'rb')
brain = pickle.load(fd, encoding="utf8")
incomincio = [x for x in brain.keys() if x[0][0] in string.ascii_uppercase]
fd.close()

application = Flask(__name__)
application.debug = LOCAL


@application.route('/')
def index():
        r = random.Random()
        seed = int(request.args.get('p', r.getrandbits(32)))
        r.seed(seed)
        previous = r.choice(incomincio)
        phrases = []
        phrases_num = 3
        curp = [previous[0], previous[1]]
        for i in range(MAX_WORDS):
            word = get_word(r, brain[previous])
            curp.append(word)
            if word.endswith("."):
                phrases.append(" ".join(curp))
                phrases_num -= 1
                if phrases_num == 0:
                    break
                previous = r.choice(incomincio)
                curp = [previous[0], previous[1]]
            else:
                previous = (previous[1], word)
        frasa = phrases[0].encode('ascii', 'xmlcharrefreplace').decode()
        return render_template("home.html", robba=frasa, seed=seed)


@application.route('/corpora.pk')
def static_from_root():
    return send_from_directory(os.path.dirname(__file__), CORPORA)

@application.route('/corpora.txt')
def static_from_root2():
    return send_from_directory(os.path.dirname(__file__), "corpora.txt")

#
# Below for testing only
#
if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    httpd = make_server('localhost', 8051, application)
    httpd.serve_forever()
